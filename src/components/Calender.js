import React, {Component} from 'react';
import Month from './Month';

class Calender extends Component {

    //use set interval to set day

    monthData = {
        0: {
            name: "January",
            length: 31
        },
        1: {
            name: "February",
            length: 28
        },
        2: {
            name: "March",
            length: 31
        },
        3: {
            name: "April",
            length: 30
        },
        4: {
            name: "May",
            length: 31
        },
        5: {
            name: "June",
            length: 30
        },
        6: {
            name: "July",
            length: 31
        },
        7: {
            name: "August",
            length: 31
        },
        8: {
            name: "September",
            length: 30
        },
        9: {
            name: "October",
            length: 31
        },
        10: {
            name: "November",
            length: 30
        },
        11: {
            name: "December",
            length: 31
        }
    };

    freshDate = new Date();
    currentDay = this.freshDate.getDate();
    firstDayOfMonth = new Date(`${this.monthData[parseInt(this.freshDate.getMonth(), 10)].name} 1, ${this.freshDate.getFullYear()}`).getDay();


    //pass current month for first render

    //have a next and previous event to pass next and previouse month data

    

    render = () => {
        return(
            <div className="calender">
                <Month firstDayOfMonth={this.firstDayOfMonth} freshDate={this.freshDate} monthData={this.monthData}/>
            </div>
        );
    }
}

export default Calender