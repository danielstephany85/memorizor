import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Day from './Day';

class Month extends Component {

    dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    monthGridSpaces = 42;
    prevMonth;
    currentMonth;
    nextMonth;
    days = [];

    componentWillMount = () => {
        this.setMonths();
        // console.log(this.prevMonth,this.currentMonth,this.nextMonth);
        this.setDays();
    }

    setMonths = () => {
        this.currentMonth = parseInt(this.props.freshDate.getMonth(),10);
        if (this.currentMonth === 11){
            this.nextMonth = 0;
        }else {
            this.nextMonth = this.currentMonth + 1;
        }
        if (this.currentMonth === 0) {
            this.prevMonth = 11;
        } else {
            this.prevMonth = this.currentMonth - 1;
        }
    }

    setDays = () => {
        // this.firstDayOfMonth = 6;
        console.log(this.props.firstDayOfMonth);
        let prevMonthLength = this.props.monthData[this.prevMonth].length;
        const currentMonthLength = this.props.monthData[this.currentMonth].length;
        let prevMonthIndex = prevMonthLength - this.props.firstDayOfMonth + 1;
        let currentMonthIndex = 1;
        let nextMonthIndex = 1;

        //set current month days and next

        for(let i = 1; i <= this.monthGridSpaces; i++){
            //set prev month days
            if (this.props.firstDayOfMonth > 0 && i <= this.props.firstDayOfMonth) {
                //prev month
                console.log(prevMonthIndex);
                this.days.push(<Day day={prevMonthIndex} key={i}/>);
                prevMonthIndex ++;
            } else if ((i - this.props.firstDayOfMonth) >= 0 && (i - this.props.firstDayOfMonth) <= currentMonthLength ){
                //current month
                console.log(currentMonthIndex);
                this.days.push(<Day day={currentMonthIndex} key={i}/>);
                currentMonthIndex ++;
            } else {
                //next month
                console.log(nextMonthIndex);
                this.days.push(<Day day={nextMonthIndex} key={i} />);
                nextMonthIndex ++;
            }

        }
    }

    render = () => {


        return(
            <React.Fragment>
                <h1>{this.props.monthData[this.currentMonth].name}</h1>
                <div className='month'>
                    <div className="month__day-labels">
                        <div>Sunday</div>
                        <div>Monday</div>
                        <div>Tuesday</div>
                        <div>Wednesday</div>
                        <div>Thursday</div>
                        <div>Friday</div>
                        <div>Saturday</div>
                    </div>
                    {this.days}
                </div>
            </React.Fragment>
        );
    }
}

Month.propTypes = {
    monthData: PropTypes.object.isRequired,
    freshDate: PropTypes.object.isRequired,
    firstDayOfMonth: PropTypes.number.isRequired
}

export default Month;