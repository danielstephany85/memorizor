import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Day extends Component {

    render = () => {
        return(
            <div className="day">
            <span>{this.props.day}</span>

            </div>
        );
    }
}

Day.propTypes = {
    day: PropTypes.number
}

export default Day;